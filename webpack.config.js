'use strict';

var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

  // Sets relative root directory for "entry" key
  context: path.resolve('js'),

  // Application entrypoint
  entry: ['./app'],

  output: {
    path: path.resolve('build/js/'),    // directory where bundle will go
    publicPath: '/public/assets/js/',   // where dev server will serve bundle from, matches index.html
    filename: 'bundle.js'
  },

  plugins: [
    new ExtractTextPlugin('style.css')
  ],

  // So that requests for root / will serve public/index.html
  devServer: {
    contentBase: 'public'
  },

  module: {
    loaders: [
      // sass
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader?sourceMap!sass-loader?sourceMap')
      },
      // images less than limit will be inlined (into bundle.js) and turned into base64 encoded data,
      // images greater than limit will be created as separate image and will be a separate request
      {
        test: /\.(png|jpg)$/,
        exclude: /node_modules/,
        loader: 'url-loader?limit=100'
      }
    ]
  },

  // specify what kind of files can be loaded without having to specify their extensions
  resolve: {
    extensions: ['', '.js', '.es6']
  }
};
