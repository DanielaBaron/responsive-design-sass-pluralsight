<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [Hands on Responsive Design Using a CSS Preprocessor](#markdown-header-hands-on-responsive-design-using-a-css-preprocessor)
  - [Introduction to SASS](#markdown-header-introduction-to-sass)
    - [Partials and Imports](#markdown-header-partials-and-imports)
    - [Variables](#markdown-header-variables)
    - [Nesting](#markdown-header-nesting)
    - [Mixins](#markdown-header-mixins)
    - [Extend / Inheritance](#markdown-header-extend--inheritance)
    - [Math Operations](#markdown-header-math-operations)
    - [Control Directives](#markdown-header-control-directives)
  - [Project Setup](#markdown-header-project-setup)
    - [CSS Reset](#markdown-header-css-reset)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Hands on Responsive Design Using a CSS Preprocessor

> Learning Responsive Design using SASS with Pluralsight [course](https://app.pluralsight.com/library/courses/hands-on-responsive-design-css-preprocessor/table-of-contents)

## Introduction to SASS

### Partials and Imports

Partials allow for dividing CSS into multiple files for development. Named starting with underscore, for example `_phone-default.scss`, `tablet.scss`, etc.

Imports allow the partials to be combined into one file for production. When importing, do not specify underscore or file extension, for example:

```css
/* styles.scss */
@import "phone-default";
@import "tablet";
@import "desktop";
```

### Variables
Use semantic variable names. NOT this: `$orange: #f15b2a;` because color value could change for example `$orange: #5c519d;`.
Do this instead `$primary-color: #f15b2a;`

Another option is to create two deep variable names to get both color and semantic names. For example:

```css
/* COLORS */
$red: #FF0000;
$blue: #0000FF;

$page-title-color: $red;
$link-color: $blue;
$menu-background: $red;
$footer-background: $blue;
```

Variables also work well for font weights and gutters:

```css
/* FONT WEIGHT */
$normal: 400;
$heavy: 700;

/* FONT WEIGHT */
$gutter: 2%;
```

### Nesting

Can nest code similar to how html is written:

```css
nav {
  ul {
    list-style: none;
  }
  li {
    float: left;
  }
  a {
    display: block;
    padding: .5em 1em;
    text-decoration: none;
  }
}
```

### Mixins

Sort of like a function. Place mixins at the start of the .scss file then call it from anywhere.

```css
@mixin its-name ($variable1) {
  property: value;
  property: $variable1;
  property: value;
}
```

Without mixins, for example brown button with orange text and orange button with brown text, get a lot of duplication:

```css
/* COLORS */
$brown: #86461e;
$orange: #ecbc4d;

.button1 {
  border-radius: 20px;
  border: 1px solid #000;
  width: 10em;
  padding: .5em;
  box-shadow: 2px 2px 10px #999;
  text-align: center;
  color: $brown;
  background-color: $orange;
}
.button2 {
  border-radius: 20px;
  border: 1px solid #000;
  width: 10em;
  padding: .5em;
  box-shadow: 2px 2px 10px #999;
  text-align: center;
  color: $orange;
  background-color: $brown;
}
```

With mixin:

```css
/* COLORS */
$brown: #86461e;
$orange: #ecbc4d;

@mixin button($color, $background) {
  border-radius: 20px;
  border: 1px solid #000;
  width: 10em;
  padding: .5em;
  box-shadow: 2px 2px 10px #999;
  text-align: center;
  color: $color;
  background-color: $background;
}

.button1 {
  @include button($brown, $orange)
}
.button2 {
  @include button($orange, $brown)
}
```

### Extend / Inheritance

Can be messy and not always recommended, but can improve DRY. Example:

```css
.foo1 {
  padding: .25em;
  border: 1px solid #fc519d;
  color: red;
}

.foo2 {
  @extend .foo1;
  color: green;
}
```

Compiles to:

```css
.foo1, .foo2 {
  padding: .25em;
  border: 1px solid #fc519d;
  color: red;
}

.foo2 {
  color: green;
}
```

Note `foo1` and `foo2` *combined* in first declaration block, that's `extend` at work. Overrides are added in second declaration block.

Better way to extend is to create declaration block with *only* common characteristics. This starts with `%`, which prevents it from rendering in css:

```css
%common {
  padding: .25em;
  border: 1px solid #5c519d;
}

.foo1 {
  @extend %common;
  color: red;
}

.foo2 {
  @extend %common;
  color: green;
}
```

Compiles to:

```css
.foo1, .foo2 {
  padding: .25em;
  border: 1px solid #5c519d;
}

.foo1 {
  color: red;
}

.foo2 {
  color: green;
}
```

### Math Operations

For example, to calculate column widths:

```css
.width3 {
  width: 3 / 12 * 100;
}
```

Compiles to:

```css
.width3 {
  width: 25%;
}
```

Spacing out images:

```css
$gutter: 2%

.graphic4 {
  width: 4 / 12 * 100%;
  float: right;
}

.graphic4 {
  width: (4 / 12 * 100%) - $gutter;
  float: right;
  margin-left: $gutter;
}
```

Compiles to:

```css
.graphic4 {
  width: .313333%;
  float: right;
  margin-left: 2%;
}
```

### Control Directives

For more advanced use, `if`, `for`, `each`, and `while`.

For example, use `for` to generate progressively lighter background:

```css
$brown: #86461E;

@for $i from 1 through 6 {
  $howmuch: $i*10%;
  .shade#{$i} {
    background-color: lighten($brown, $howmuch);
  }
}
```

Compiles to:

```css
.shade1{ background-color: #b05c27; }
.shade2{ background-color: #d27338; }
.shade3{ background-color: #dc9061; }
.shade4{ background-color: #e5ae8b; }
.shade5{ background-color: #eecbb5; }
.shade6{ background-color: #f8e8de; }
```

## Project Setup

Partials are named starting with underscore, for example `_phone-default.scss`, `_table.scss`, `_desktop.scss`. Then imported into a single file not named with underscore, for example `style.scss`.

Since its mobile first, the phone styles are the default and loaded first, there is no media query there. Each successive partial (tablet, desktop) has in increasing min-width media query to match increasing screen sizes.

To set tablet media query min width, rather than 720px, divide by default base font size of 16px to get 45em:

```css
@media only screen and(min-width: 45em) {

}/* end query */
```

For desktop, 1028px/16px = 64.25em.

### CSS Reset

Add `_reset.scss` to styles with [content from here](http://meyerweb.com/eric/tools/css/reset/).
